from django.contrib import admin

from imggallery.models import Image, Tag


class ImageAdmin(admin.ModelAdmin):
    ordering = ['pk']
    autocomplete_fields = ['alias_of', 'comes_from']
    search_fields = ['pk']


admin.site.register(Image, ImageAdmin)
admin.site.register(Tag)
