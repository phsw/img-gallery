from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse


class Tag(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = _('tag')
        verbose_name_plural = _('tags')

    def __str__(self):
        return self.name


class Image(models.Model):
    comment = models.TextField(blank=True)
    alias_of = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, blank=True, related_name="alias_of_set")
    comes_from = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, blank=True, related_name="comes_from_set")
    filepath = models.ImageField(upload_to="data/", max_length=255)
    tags = models.ManyToManyField(Tag, blank=True)

    def get_absolute_url(self):
       return reverse('imageview', args=[self.id])
