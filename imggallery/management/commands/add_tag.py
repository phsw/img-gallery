from django.core.management.base import BaseCommand
from django.db import transaction

from imggallery.models import Image, Tag


class Command(BaseCommand):
    help = 'Add a tag to several images.'

    # From https://github.com/django/django/blob/59afe61a970dd60df388e7cda9041ef3c0e770cb/django/db/migrations/questioner.py#L87
    def _boolean_input(self, question, default=None):
        result = input("%s " % question)
        if not result and default is not None:
            return default
        while len(result) < 1 or result[0].lower() not in "yn":
            result = input("Please answer yes or no: ")
        return result[0].lower() == "y"

    def add_arguments(self, parser):
        parser.add_argument('tag_id', type=int)
        parser.add_argument('images', type=str)

    def handle(self, *args, **options):
        tag = Tag.objects.get(pk=options['tag_id'])
        images_pk = options['images'].split(',')

        print("Will add the tag '" + tag.name + "'")

        with transaction.atomic():
            for image_pk in images_pk:
                image = Image.objects.get(pk=image_pk)
                image.tags.add(tag)
                image.save()

            if not self._boolean_input("Is it OK ? [Y/n]", default="y"):
                raise Exception("Cancelled by user.")

        self.stdout.write(self.style.SUCCESS('Successfully done.'))
