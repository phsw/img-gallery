# Generated by Django 3.1 on 2020-08-16 10:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('imggallery', '0005_auto_20200816_0930'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.AddField(
            model_name='image',
            name='tags',
            field=models.ManyToManyField(blank=True, to='imggallery.Tag'),
        ),
    ]
