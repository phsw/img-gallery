from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView
from django.conf import settings
from django.shortcuts import render

from imggallery.models import Image
from imggallery.forms import ImageSearchForm


class ImageListView(ListView):
    template_name = 'image_list.html'
    model = Image
    queryset = Image.objects.filter(alias_of=None, comes_from_set=None).order_by('pk')
    paginate_by = settings.PAGINATE_BY


class ImageDetailView(DetailView):
    template_name = "image_detail.html"
    model = Image

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.object.alias_of is None:
            page_num = (Image.objects.filter(alias_of=None, pk__lt=self.object.pk).count() // settings.PAGINATE_BY) + 1
            context['back_url'] = reverse_lazy('homepage') + "?page=" + str(page_num)
        else:
            context['back_url'] = reverse_lazy('imageview', kwargs={'pk': self.object.alias_of.pk})

        context['aliases'] = Image.objects.filter(alias_of=self.object).order_by('pk')
        context['decomposition'] = Image.objects.filter(comes_from=self.object).order_by('pk')

        return context


class ImageSearchView(FormView):
    template_name = "image_search.html"
    form_class = ImageSearchForm

    def form_valid(self, form):
        context = self.get_context_data()

        context['results'] = Image.objects.filter(tags__in=form.cleaned_data['tags']).distinct().order_by('pk')

        return render(self.request, self.template_name, context)
